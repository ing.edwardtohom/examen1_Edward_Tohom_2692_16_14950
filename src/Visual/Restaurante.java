package Visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class Restaurante extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nombre;
    private JTextField direccion;
    private JTable table1;
    private JButton salirButton;

    private DefaultTableModel model;

    public Restaurante() {
        this.setSize(700,500);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        model=new DefaultTableModel();
        model.addColumn("Nombre de Restaurante");;
        model.addColumn("Direccion de Restaurante");
        table1.setModel(model);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void onOK() {
        // add your code here
       // dispose();
        String nombreVar=nombre.getText();
        String direccionVar=direccion.getText();
        nombre.setText("");
        direccion.setText("");
        model.addRow(new Object[]{nombreVar, direccionVar});

    }

    private void onCancel() {
        // add your code here if necessary
       // dispose();
        model.removeRow(table1.getSelectedRow());
    }

    public static void main(String[] args) {
        Restaurante dialog = new Restaurante();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
