package Visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class Detalle_Restaurante extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField cantTrab;
    private JTextField cantComen;
    private JTextField acepNin;
    private JTextField cantEstr;
    private JTable table1;
    private JTextField restaurante;
    private JTextField direccion;
    private JButton salirButton;
    private JTextField tipica;
    private JComboBox comboBox1;
    private JButton continuarButton;
    private JLabel acepLab;
    private JLabel cantLab;

    private DefaultTableModel model;

    public Detalle_Restaurante() {
        this.setSize(1500,500);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        comboBox1.setVisible(false);
        acepLab.setVisible(false);
        cantLab.setVisible(false);
        acepNin.setVisible(false);
        cantEstr.setVisible(false);

        model=new DefaultTableModel();
        model.addColumn("Nombre de Restaurante");
        model.addColumn("Direccion de Restaurante");
        model.addColumn("Cantidad de trabajadores");;
        model.addColumn("Cantidad de comensales");
        model.addColumn("Acepta Niños/as");
        model.addColumn("Categorias de Estrellas");
        table1.setModel(model);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        continuarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


               String tipVar=tipica.getText();
               String si="si";
                if (tipVar.equals(si)){
                    JOptionPane.showMessageDialog(null,
                            "Los campos de Acepta niños/as y cantidad de estrella se han habilitado",
                            "Informacion",
                            JOptionPane.WARNING_MESSAGE);
                acepNin.setVisible(true);
                cantEstr.setVisible(true);
                    acepLab.setVisible(true);
                    cantLab.setVisible(true);
                }
                else
                {
                    JOptionPane.showMessageDialog(null,
                            "Puedes Continuar llenando los campos de Cantidad de trabajadores y comensales",
                            "Informacion",
                            JOptionPane.WARNING_MESSAGE);

                    acepNin.setText("Null");
                    cantEstr.setText("Null");
                }


            }
        });
    }

    private void onOK() {
        // add your code here
       // dispose();
        String restVar=restaurante.getText();
        String direcVar=direccion.getText();
        int cantidaTrabajadorVar=Integer.parseInt(cantTrab.getText());
        int cantidaComensalesVar=Integer.parseInt(cantComen.getText());
        String aceptaVar=acepNin.getText();
        String catEstVar=cantEstr.getText();
        restaurante.setText("");
        direccion.setText("");
        cantTrab.setText("");
        cantComen.setText("");
        acepNin.setText("");
        cantEstr.setText("");
        tipica.setText("");
        model.addRow(new Object[]{restVar, direcVar, cantidaTrabajadorVar, cantidaComensalesVar, aceptaVar, catEstVar});

        comboBox1.setVisible(false);
        acepLab.setVisible(false);
        cantLab.setVisible(false);
        acepNin.setVisible(false);
        cantEstr.setVisible(false);


    }

    private void onCancel() {
        // add your code here if necessary
       // dispose();
        model.removeRow(table1.getSelectedRow());
    }

    public static void main(String[] args) {
        Detalle_Restaurante dialog = new Detalle_Restaurante();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
